package com.nullpoint.pos9.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.nullpoint.pos9.db.ProductDao
import com.nullpoint.pos9.vo.Product
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Abstraction for accessing Product data.
 */
@Singleton
class ProductRepository @Inject constructor(private val productDao: ProductDao) {
    @WorkerThread
    fun getAllProduct(): LiveData<List<Product>> {
        return productDao.getAllProduct()
    }

    @WorkerThread
    fun getProductById(id: Int): LiveData<Product> {
        return productDao.findProductById(id)
    }

    @WorkerThread
    fun insertToDb(name: String, price: Long, path: String) {
        val item = Product()
        item.name = name
        item.price = price
        item.imagePath = path

        val res = productDao.insertProduct(item)
        if (res < 1) {
            //no rows affected
        }
    }

    @WorkerThread
    fun updateNameById(id: Int, name: String) {
        productDao.updateNameById(id, name)
    }

    @WorkerThread
    fun updatePriceById(id: Int, price: Int) {
        productDao.updatePriceById(id, price)

        // check if product exist
        // var cPrice: Int
    }
}
