package com.nullpoint.pos9.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.nullpoint.pos9.db.OrderDao
import com.nullpoint.pos9.db.OrderItemDao
import com.nullpoint.pos9.ui.monthlyReport.MonthlySummary
import com.nullpoint.pos9.vo.Order
import com.nullpoint.pos9.vo.OrderItem
import org.threeten.bp.YearMonth
import org.threeten.bp.ZoneId
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Abstraction for accessing Order data.
 */
@Singleton
class OrderRepository @Inject constructor(
    private val orderDao: OrderDao,
    private val orderItemDao: OrderItemDao
) {
    @WorkerThread
    fun getAllOrder(): LiveData<List<Order>> {
        return orderDao.getAllOrder()
    }

    fun isOrderEmpty(): LiveData<Boolean> {
        return Transformations.map(orderDao.getOrderCount()) {
            it == 0
        }
    }

    fun getAllOrderSync(): List<Order> {
        return orderDao.getAllOrderList()
    }

    fun saveOrder(cart: MutableLiveData<List<OrderItem>>) {
        cart.value?.let { cartItems ->
            val totalPrice = cartItems.map { it.totalPrice }.reduce { acc, price -> acc + price }
            val order = Order(actualPayAmount = totalPrice, totalPayAmount = totalPrice)
            val rowid = orderDao.insetOrder(order)
            orderItemDao.insetOrderItems(cartItems.map { it.apply { idOrder = rowid.toInt() } })

        }
    }

    fun getMonthlySummary(): LiveData<List<MonthlySummary>> = Transformations.map(getAllOrder()) { order ->
        order.groupBy {
            val zonedDate = it.createdAt.atZone(ZoneId.systemDefault())
            YearMonth.from(zonedDate)
        }.map {
            MonthlySummary(it.value, it.key)
        }
    }
}
