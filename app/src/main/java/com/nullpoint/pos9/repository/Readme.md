# Repository

This files handles the retrieval of data, wheter it's from the network or db or sharedpreference. The
rest of the app shouldn't care where the data is coming from, they should only care about whether they
get the correct data or not. The repositories abstracts away all the data retrieval, caching, syncing,
etc...

See: [Repository Pattern](https://medium.com/@krzychukosobudzki/repository-design-pattern-bc490b256006)