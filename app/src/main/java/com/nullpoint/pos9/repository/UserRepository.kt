package com.nullpoint.pos9.repository

import javax.inject.Inject
import javax.inject.Singleton

import com.nullpoint.pos9.db.UserDao
/**
 * Abstraction for accessing User data.
 */
@Singleton
class UserRepository @Inject constructor (private val userDao: UserDao) {

}