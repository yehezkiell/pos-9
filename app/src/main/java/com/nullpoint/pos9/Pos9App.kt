package com.nullpoint.pos9

import android.app.Activity
import androidx.multidex.MultiDexApplication
import com.bugsnag.android.Bugsnag
import com.facebook.stetho.Stetho
import com.jakewharton.threetenabp.AndroidThreeTen
import com.mixpanel.android.mpmetrics.MixpanelAPI
import com.nullpoint.pos9.Pos9App.STATIC.MIXPANEL_TOKEN
import com.nullpoint.pos9.di.AppInjector
import com.survicate.surveys.Survicate
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class Pos9App : MultiDexApplication(), HasActivityInjector {
    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    object STATIC {
        const val MIXPANEL_TOKEN = "c8ef08b4b1295bc908d7cefa907768c2"
    }

    override fun activityInjector() = dispatchingActivityInjector

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
        Stetho.initializeWithDefaults(this)
        AppInjector.init(this)
        Survicate.init(this)
        Bugsnag.init(this)
        MixpanelAPI.getInstance(this, MIXPANEL_TOKEN)
    }
}
