package com.nullpoint.pos9.utils

import androidx.databinding.InverseMethod
import org.threeten.bp.Instant
import org.threeten.bp.YearMonth
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle
import java.text.NumberFormat
import java.util.Locale

/**
 * Static class for converting values, used mostly for converting values to be displayed on the UI, ex Date object
 * into a formatted string.
 * */

object Converter {
    private val numberFormatter by lazy {
        NumberFormat.getNumberInstance(Locale("in", "ID"))
    }

    @JvmStatic
    fun instantToString(date: Instant): String = DateTimeFormatter
        .ofLocalizedDate(FormatStyle.FULL)
        .withLocale(Locale("ID"))
        .format(date.atZone(ZoneId.systemDefault()))

    @JvmStatic
    fun yearMonthToString(date: YearMonth) = """
        ${date.month.toString().toLowerCase().capitalize()} ${date.year}
    """.trimIndent()

    @JvmStatic
    fun longToStringCurrency(amount: Long): String = NumberFormat
        .getCurrencyInstance(Locale("in", "ID"))
        .format(amount)

    /** Convert a number string (123456) into comma format (123.456) */
    @JvmStatic
    @InverseMethod("commaFormatToString")
    fun stringToCommaFormat(value: String?): String =
        if (value == null || value == "") ""
        else numberFormatter.format(value.toLong())

    @JvmStatic
    fun commaFormatToString(value: String?): String =
        if (value == null || value == "") ""
        else numberFormatter.parse(value).toString()
}
