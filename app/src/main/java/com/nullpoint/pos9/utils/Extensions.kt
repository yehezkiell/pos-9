package com.nullpoint.pos9.utils

import com.google.android.material.bottomnavigation.BottomNavigationView

fun BottomNavigationView.setSelectedItemId(itemId: Int, triggerAction: Boolean) {
    if (triggerAction) this.selectedItemId = itemId
    else this.menu.findItem(itemId).isChecked = true
}
