package com.nullpoint.pos9.utils

import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import com.google.android.material.textfield.TextInputEditText

class EditTextNumberFormatter(private val editText: TextInputEditText) : TextWatcher, Runnable {
    var lastCursorLocation = 0
    var lengthBefore = 0
    var lengthAfter = 0
    var isKeyDelete = false
    var delay: Long = 100
    var lastEdit: Long = 0
    var handler = Handler()

    init {
        // This is triggered before registered TextWatcherDetect is called, so we record
        // isKeyDelete here so we can know whether text change was triggered by delete
        // key or not
        editText.setOnKeyListener { _, keyCode, event ->
            isKeyDelete = keyCode == KeyEvent.KEYCODE_DEL
            if (event!!.action == KeyEvent.ACTION_UP) {
                handler.postDelayed(this, delay)
            }
            false
        }
    }

    // Updates text value only if `delay` amount of time has passed since
    // last text changed?
    override fun run() {
        if (System.currentTimeMillis() > lastEdit + delay && isKeyDelete) {
            val value = editText.text.toString()
            if (value != "") {
                val cleanedText = Converter.commaFormatToString(value)
                val formattedText = Converter.stringToCommaFormat(cleanedText)
                editText.setText(formattedText)
            }
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        lastCursorLocation = editText.selectionStart
        lengthBefore = s?.length ?: 0
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        handler.removeCallbacks(this, delay)
        if (s.isNullOrEmpty()) return

        // Figure out where cursor should be placed next
        lengthAfter = s.length
        val selectionOffset = lengthAfter - lengthBefore
        editText.setSelection(lastCursorLocation + selectionOffset)
        lastCursorLocation = editText.selectionStart

        // Reformat text unless text change was triggered by delete key
        lastEdit = System.currentTimeMillis()
        if (isKeyDelete) return
        // TODO: this repeats with the run block, why? Can it be simplified?
        val cleanedText = Converter.commaFormatToString(s.toString())
        val formattedText = Converter.stringToCommaFormat(cleanedText)
        if (lengthAfter != lengthBefore) {
            editText.setText(formattedText)
        }
    }

    override fun afterTextChanged(s: Editable?) {
        /* Nothing to do here */
    }
}