package com.nullpoint.pos9.utils

import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputEditText

object BindingAdapters {
    @JvmStatic
    @BindingAdapter("bind:textChangedListener")
    fun bindTextWatcher(view: TextInputEditText, textWatcher: TextWatcher) {
        view.addTextChangedListener(textWatcher)
        Log.d("posix", "length:"+view.text.toString().length)
    }

    @JvmStatic
    @BindingAdapter("bind:onKeyListener")
    fun bindKeyListener(view: TextInputEditText, listener: View.OnKeyListener) {
        view.setOnKeyListener(listener)
    }
}