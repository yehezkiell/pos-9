package com.nullpoint.pos9.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.ContextWrapper
import android.os.Build
import androidx.core.app.NotificationCompat
import com.nullpoint.pos9.R

internal class NotificationHelper(ctx: Context) : ContextWrapper(ctx) {
    val CHANNEL_NAME = "report"

    private val manager: NotificationManager by lazy {
        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    init {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(PRIMARY_CHANNEL, CHANNEL_NAME, importance)
            // Register the channel with the system
            manager.createNotificationChannel(channel)
        }
    }

    fun getNotificationPrimaryChannel(title: String, body: String): NotificationCompat.Builder {
        return NotificationCompat.Builder(applicationContext, PRIMARY_CHANNEL)
            .setContentTitle(title)
            .setContentText(body)
            .setSmallIcon(R.mipmap.ic_notif_icon)
            .setAutoCancel(true)
    }

    fun notify(id: Int, notification: NotificationCompat.Builder) {
        manager.notify(id, notification.build())
    }

    companion object {
        const val PRIMARY_CHANNEL = "default"
    }
}