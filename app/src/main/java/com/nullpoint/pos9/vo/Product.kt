package com.nullpoint.pos9.vo

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.threeten.bp.Instant
import java.util.Date

@Entity()
data class Product(
    @PrimaryKey(autoGenerate = true)
    val idProduct: Int? = null,
    var name: String = "",
    var price: Long = 0,
    var imagePath: String = "",
    var createdAt: Instant = Instant.now(),
    var updatedAt: Instant  = Instant.now()
)
