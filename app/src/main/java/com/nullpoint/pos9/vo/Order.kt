package com.nullpoint.pos9.vo

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import org.threeten.bp.Instant
import java.util.Date

@Entity(
    foreignKeys = [ForeignKey(
        entity = Customer::class,
        parentColumns = ["idCustomer"],
        childColumns = ["idCustomer"],
        onUpdate = ForeignKey.CASCADE
    )],
    indices = [Index("idCustomer")]
)
data class Order(
    @PrimaryKey(autoGenerate = true)
    val idOrder: Int? = null,
    val idCustomer: Int? = null,
    val idUser: Int? = null,
    val totalPayAmount: Long,
    val tax: Float = 0f,
    val actualPayAmount: Long,
    val createdAt: Instant = Instant.now(),
    val updatedAt: Instant = Instant.now(),
    val completed: Boolean = true
)
