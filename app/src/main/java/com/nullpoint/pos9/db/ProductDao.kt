package com.nullpoint.pos9.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.nullpoint.pos9.vo.Product

/** Used for querying Product table from DB. */
@Dao
interface ProductDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProduct(product: Product): Long

    @Query("select * from product where name = :name")
    fun findProductByName(name: String): Product?

    @Query("select * from product where idProduct = :id")
    fun findProductById(id: Int): LiveData<Product>

    @Query("select * from product order by name asc")
    fun getAllProduct(): LiveData<List<Product>>

    @Query("delete from product where idProduct = :id")
    fun deleteProductById(id: Int): Int

    @Query(
        """
        update product
        set price = :price
        where idProduct = :id
        """
    )
    fun updatePriceById(id: Int, price: Int): Long

    @Query(
        """
        update product
        set name = :name
        where idProduct = :id
        """
    )
    fun updateNameById(id: Int, name: String): Long
}
