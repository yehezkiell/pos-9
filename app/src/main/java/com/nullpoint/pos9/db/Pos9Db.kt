package com.nullpoint.pos9.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.nullpoint.pos9.vo.Customer
import com.nullpoint.pos9.vo.Order
import com.nullpoint.pos9.vo.OrderItem
import com.nullpoint.pos9.vo.Product
import com.nullpoint.pos9.vo.User

/** RoomDatabase definition.
 *
 * Used for setting up Room for Pos9.
 * */
@Database(
    entities = [
        Customer::class,
        Order::class,
        OrderItem::class,
        Product::class,
        User::class
    ],
    version = 4,
    exportSchema = false
)
@TypeConverters(InstantTypeConverter::class)
abstract class Pos9Db : RoomDatabase() {
    abstract fun customerDao(): CustomerDao
    abstract fun orderDao(): OrderDao
    abstract fun orderItemDao(): OrderItemDao
    abstract fun productDao(): ProductDao
    abstract fun promoDao(): PromoDao
    abstract fun userDao(): UserDao
}
