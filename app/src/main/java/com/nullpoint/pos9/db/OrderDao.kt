package com.nullpoint.pos9.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

import com.nullpoint.pos9.vo.Order
import org.threeten.bp.Instant

/** Used for querying Order table from DB. */
@Dao
interface OrderDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insetOrder(order: Order): Long

    @Query("select * from 'order' where idOrder = :id")
    fun findOrderById(id: Int): Order?

    @Query("select * from 'order' order by createdAt asc")
    fun getAllOrder(): LiveData<List<Order>>

    @Query("select * from 'order' where createdAt >= :dateStart and createdAt < :dateEnd")
    fun getOrderByDateList(dateStart: Instant, dateEnd: Instant): List<Order>

    @Query("select * from 'order' order by createdAt asc")
    fun getAllOrderList(): List<Order>

    @Query("delete from 'order' where idOrder = :id")
    fun deleteOrderById(id: Int): Int

    @Query("select * from 'order' where createdAt >= :dateStart and createdAt < :dateEnd")
    fun getOrderByDate(dateStart: Instant, dateEnd: Instant): LiveData<List<Order>>

    @Query("select count(*) from `Order`")
    fun getOrderCount(): LiveData<Int>
}
