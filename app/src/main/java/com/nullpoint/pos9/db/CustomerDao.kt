package com.nullpoint.pos9.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

import com.nullpoint.pos9.vo.Customer

/** Used for querying Customer table from DB. */
@Dao
interface CustomerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCustomer(customer: Customer): Long

    @Query("select * from customer where idCustomer = :id")
    fun findCustomerById(id: Int): Customer?

    @Query("select * from customer order by name asc")
    fun getAllCustomer(): LiveData<List<Customer>>

    @Query("delete from customer where idCustomer = :id")
    fun deleteCustomerById(id: Int): Int
}
