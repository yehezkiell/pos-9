package com.nullpoint.pos9.di

/** Indicate that fragment is injectable by Dagger. */
interface Injectable
