package com.nullpoint.pos9.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.nullpoint.pos9.db.OrderDao
import com.nullpoint.pos9.db.OrderItemDao
import com.nullpoint.pos9.db.Pos9Db
import com.nullpoint.pos9.db.ProductDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 *  This module provides the singleton dependencies that the app requires (dao, db, etc).
 */
@Module(includes = [ViewModelModule::class])
class AppModule {

    /**
     * Provides DB instance.
     */
    @Singleton
    @Provides
    fun provideDb(app: Application): Pos9Db = Room
        .databaseBuilder(app, Pos9Db::class.java, "pos9.db")
        .fallbackToDestructiveMigration()
        .build()

    @Singleton
    @Provides
    fun provideProductDao(db: Pos9Db): ProductDao = db.productDao()

    @Singleton
    @Provides
    fun provideOrderDao(db: Pos9Db): OrderDao = db.orderDao()

    @Singleton
    @Provides
    fun provideOrderItemDao(db: Pos9Db): OrderItemDao = db.orderItemDao()

    @Singleton
    @Provides
    fun provideSharedPreference(app: Application): SharedPreferences =
        app.getSharedPreferences("deca", Context.MODE_PRIVATE)
}
