package com.nullpoint.pos9.di

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.nullpoint.pos9.Pos9App
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector

/**
 * Helper class for injecting dependency into fragment and activity that will not depend on
 * the activity/fragment lifecycle
 *
 * See: https://github.com/googlesamples/android-architecture-components/issues/207
 */
object AppInjector {

    /** Setup injection. */
    fun init(pos9App: Pos9App) {
        DaggerAppComponent.builder().application(pos9App)
            .build().inject(pos9App)
        pos9App.registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                handleActivity(activity)
            }

            override fun onActivityStarted(activity: Activity) { /* Nothing to do here */
            }

            override fun onActivityResumed(activity: Activity) {/* Nothing to do here */
            }

            override fun onActivityPaused(activity: Activity) {/* Nothing to do here */
            }

            override fun onActivityStopped(activity: Activity) {/* Nothing to do here */
            }

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle?) {/* Nothing to do here */
            }

            override fun onActivityDestroyed(activity: Activity) {/* Nothing to do here */
            }
        })
    }

    private fun handleActivity(activity: Activity) {
        if (activity is HasSupportFragmentInjector) {
            AndroidInjection.inject(activity)
        }
        if (activity is FragmentActivity) {
            activity.supportFragmentManager.registerFragmentLifecycleCallbacks(
                object : FragmentManager.FragmentLifecycleCallbacks() {
                    override fun onFragmentCreated(
                        fm: FragmentManager,
                        f: Fragment,
                        savedInstanceState: Bundle?
                    ) {
                        if (f is Injectable) {
                            AndroidSupportInjection.inject(f)
                        }
                    }
                }, true
            )
        }
    }
}
