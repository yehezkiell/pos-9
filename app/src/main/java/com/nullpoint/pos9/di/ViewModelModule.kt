package com.nullpoint.pos9.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nullpoint.pos9.ui.adminContainer.AdminContainerViewModel
import com.nullpoint.pos9.ui.adminMenu.AdminMenuViewModel
import com.nullpoint.pos9.ui.cashierContainer.CashierContainerViewModel
import com.nullpoint.pos9.ui.customerContainer.CustomerContainerViewModel
import com.nullpoint.pos9.ui.editProduct.EditProductViewModel
import com.nullpoint.pos9.ui.mainContainer.MainContainerViewModel
import com.nullpoint.pos9.ui.manageProducts.ManageProductsViewModel
import com.nullpoint.pos9.ui.managePromos.ManagePromosViewModel
import com.nullpoint.pos9.ui.monthlyReport.MonthlyReportViewModel
import com.nullpoint.pos9.ui.newProduct.NewProductViewModel
import com.nullpoint.pos9.ui.orderHistory.OrderHistoryViewModel
import com.nullpoint.pos9.ui.promoContainer.PromoContainerViewModel
import com.nullpoint.pos9.ui.reportContainer.ReportContainerViewModel
import com.nullpoint.pos9.ui.reportDashboard.ReportDashboardViewModel
import com.nullpoint.pos9.ui.signIn.SignInViewModel
import com.nullpoint.pos9.utils.Pos9ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Provides all of the ViewModel.
 *
 * As more viewModels are created it will be added here with @IntoMap annotation.
 * so that it will be provided to Pos9ViewModelFactory and be used in the fragments and activities.
 */
@Suppress("unused", "TooManyFunctions")
@Module
interface ViewModelModule {
    // IntoMap is a Dagger 2 multi-binding feature, for adding items into a Map.
    // See: https://google.github.io/dagger/multibindings
    /**
     * Provides SignInViewModel for usage in Pos9ViewModelFactory.
     */
    @Binds
    @IntoMap
    @ViewModelKey(SignInViewModel::class)
    fun bindSignInViewModel(viewModel: SignInViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainContainerViewModel::class)
    fun bindMainContainerVIewModel(viewModel: MainContainerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CashierContainerViewModel::class)
    fun bindCashierContainerViewModel(viewModel: CashierContainerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PromoContainerViewModel::class)
    fun bindPromoContainerViewModel(viewModel: PromoContainerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CustomerContainerViewModel::class)
    fun bindCustomerContainerViewModel(viewModel: CustomerContainerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ReportContainerViewModel::class)
    fun bindReportContainerViewModel(viewModel: ReportContainerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AdminContainerViewModel::class)
    fun bindAdminContainerViewModel(viewModel: AdminContainerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AdminMenuViewModel::class)
    fun bindAdminMenuViewModel(viewModel: AdminMenuViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ManageProductsViewModel::class)
    fun bindManageProductsViewModel(viewModel: ManageProductsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ManagePromosViewModel::class)
    fun bindManagePromosViewModel(viewModel: ManagePromosViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewProductViewModel::class)
    fun bindNewProductViewModel(viewModel: NewProductViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EditProductViewModel::class)
    fun bindEditProductViewModel(viewModel: EditProductViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ReportDashboardViewModel::class)
    fun bindReportDashboardViewModel(viewModel: ReportDashboardViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrderHistoryViewModel::class)
    fun orderHistoryViewModel(viewModel: OrderHistoryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MonthlyReportViewModel::class)
    fun bindMonthlyReportViewModel(viewModel: MonthlyReportViewModel): ViewModel

    /**
     * Provides ViewModel Factory.
     */
    @Binds
    fun bindViewModelFactory(factory: Pos9ViewModelFactory): ViewModelProvider.Factory
}
