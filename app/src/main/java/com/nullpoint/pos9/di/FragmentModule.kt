package com.nullpoint.pos9.di

import com.nullpoint.pos9.ui.adminContainer.AdminContainerFragment
import com.nullpoint.pos9.ui.adminMenu.AdminMenuFragment
import com.nullpoint.pos9.ui.cashierContainer.CashierContainerFragment
import com.nullpoint.pos9.ui.customerContainer.CustomerContainerFragment
import com.nullpoint.pos9.ui.editProduct.EditProductFragment
import com.nullpoint.pos9.ui.mainContainer.MainContainerFragment
import com.nullpoint.pos9.ui.manageProducts.ManageProductsFragment
import com.nullpoint.pos9.ui.managePromos.ManagePromosFragment
import com.nullpoint.pos9.ui.monthlyReport.MonthlyReportFragment
import com.nullpoint.pos9.ui.newProduct.NewProductFragment
import com.nullpoint.pos9.ui.orderHistory.OrderHistoryFragment
import com.nullpoint.pos9.ui.promoContainer.PromoContainerFragment
import com.nullpoint.pos9.ui.reportContainer.ReportContainerFragment
import com.nullpoint.pos9.ui.reportDashboard.ReportDashboardFragment
import com.nullpoint.pos9.ui.signIn.SignInFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * This Provides the injector for fragments
 * See: https://google.github.io/dagger/android
 */
@Suppress("unused", "TooManyFunctions")
@Module
interface FragmentModule {

    /**
     * Provides injector for SignIn fragment.
     */
    @Suppress("FunctionMaxLength")
    @ContributesAndroidInjector
    fun contributeSignInFragmentInjector(): SignInFragment

    @ContributesAndroidInjector
    fun contributeMainContainerFragmentInjector(): MainContainerFragment

    @ContributesAndroidInjector
    fun contributeCashierContainerFragmentInjector(): CashierContainerFragment

    @ContributesAndroidInjector
    fun contributePromoContainerFragmentInjector(): PromoContainerFragment

    @ContributesAndroidInjector
    fun contributeCustomerContainerFragmentInjector(): CustomerContainerFragment

    @ContributesAndroidInjector
    fun contributeReportContainerFragmentInjector(): ReportContainerFragment

    @ContributesAndroidInjector
    fun contributeAdminContainerFragmentInjector(): AdminContainerFragment

    @ContributesAndroidInjector
    fun contributeAdminMenuFragmentInjector(): AdminMenuFragment

    @ContributesAndroidInjector
    fun contributeManageProductsFragmentInjector(): ManageProductsFragment

    @ContributesAndroidInjector
    fun contributeManagePromosFragmentInjector(): ManagePromosFragment

    @ContributesAndroidInjector
    fun contributeNewProductFragmentInjector(): NewProductFragment

    @ContributesAndroidInjector
    fun contributeEditProductFragmentInjector(): EditProductFragment

    @ContributesAndroidInjector
    fun contributeReportDashboardFragmentInjector(): ReportDashboardFragment

    @ContributesAndroidInjector
    fun contributeOrderHistoryFragmentInjector(): OrderHistoryFragment

    @ContributesAndroidInjector
    fun contributeMonthlyReportFragmentInjector(): MonthlyReportFragment
}
