package com.nullpoint.pos9.ui.manageProducts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.nullpoint.pos9.AppExecutors
import com.nullpoint.pos9.R
import com.nullpoint.pos9.databinding.ManageProductsFragmentBinding
import com.nullpoint.pos9.di.Injectable
import com.nullpoint.pos9.ui.common.ProductListAdapter
import com.nullpoint.pos9.utils.autoCleared
import javax.inject.Inject

class ManageProductsFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var viewModel: ManageProductsViewModel
    private var adapter by autoCleared<ProductListAdapter>()
    var binding by autoCleared<ManageProductsFragmentBinding>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = ManageProductsFragmentBinding.inflate(inflater, container, false)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ManageProductsViewModel::class.java)

        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.addProductsButton.setOnClickListener {
            findNavController().navigate(R.id.manage_products_to_new_product)
        }

        adapter = ProductListAdapter(appExecutors) {
            val direction = ManageProductsFragmentDirections
                .manageProductsToEditProduct()
                .setId(it.idProduct ?: -1)
            findNavController().navigate(direction)
        }
        binding.adminProductRecyclerview.adapter = adapter
        viewModel.products.observe(this, Observer {
            adapter.submitList(it)
        })
        return binding.root
    }
}
