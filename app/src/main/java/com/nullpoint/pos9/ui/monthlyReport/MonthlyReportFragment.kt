package com.nullpoint.pos9.ui.monthlyReport

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.nullpoint.pos9.AppExecutors
import com.nullpoint.pos9.databinding.MonthlyReportFragmentBinding
import com.nullpoint.pos9.di.Injectable
import com.nullpoint.pos9.utils.autoCleared
import javax.inject.Inject

class MonthlyReportFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var viewModel: MonthlyReportViewModel

    var binding by autoCleared<MonthlyReportFragmentBinding>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = MonthlyReportFragmentBinding.inflate(inflater, container, false)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(MonthlyReportViewModel::class.java)

        // TODO: Implement monthly summary details on click
        val adapter = MonthlySummaryListAdapter(appExecutors)
        binding.monthlySummaryList.adapter = adapter
        viewModel.ordersByMonth.observe(this, Observer {
            if (it != null && it.isNotEmpty()) adapter.submitList(it)
        })
        binding.viewModel = viewModel
        return binding.root
    }
}