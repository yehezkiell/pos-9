package com.nullpoint.pos9.ui.reportContainer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.nullpoint.pos9.R
import com.nullpoint.pos9.di.Injectable
import kotlinx.android.synthetic.main.report_container_fragment.*
import javax.inject.Inject

class ReportContainerFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ReportContainerViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.report_container_fragment, container, false)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ReportContainerViewModel::class.java)
        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
        val title = view.findViewById<TextView>(R.id.toolbar_title)
        val navHost = view.findViewById<View>(R.id.bottom_nav_child_host_fragment)
        val navController = Navigation.findNavController(navHost)
        navController.addOnDestinationChangedListener { _, destination, _ ->
           title.text = destination.label
        }
        toolbar.setupWithNavController(navController)
        return view
    }

    override fun onResume() {
        super.onResume()
        viewModel.isOrderEmpty.observe(this, Observer {
            empty_switcher.displayedChild = if (it) 0 else 1
        })
    }
}

