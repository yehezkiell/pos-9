package com.nullpoint.pos9.ui.cashierContainer

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.nullpoint.pos9.AppExecutors
import com.nullpoint.pos9.databinding.CartProductItemBinding
import com.nullpoint.pos9.ui.common.DataBoundListAdapter
import com.nullpoint.pos9.vo.OrderItem

/**
 *  Adapter for list of item on cart.
 * */
class CartProductsAdapter(
    appExecutors: AppExecutors, private val callback: ((OrderItem) -> Unit)?
) : DataBoundListAdapter<OrderItem, CartProductItemBinding>(
    appExecutors, OrderItemDiffUtil()
) {
    override fun createBinding(parent: ViewGroup): CartProductItemBinding {
        val binding = CartProductItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        // TODO: We might not need two different watcher
        binding.inputQtyEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString() == "0") binding.inputQtyEditText.setText("1")
                binding.item?.let {
                    callback?.invoke(it)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                /* Nothing to do here */
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                /* Nothing to do here */
            }
        })
        binding.inputQtyEditText.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus && binding.inputQtyEditText.text.toString() == "")
                binding.inputQtyEditText.setText("1")
        }
        return binding
    }

    override fun bind(binding: CartProductItemBinding, item: OrderItem) {
        binding.item = item
    }

    public override fun getItem(position: Int): OrderItem {
        return super.getItem(position)
    }
}

class OrderItemDiffUtil : DiffUtil.ItemCallback<OrderItem>() {
    override fun areContentsTheSame(oldItem: OrderItem, newItem: OrderItem) = oldItem == newItem

    override fun areItemsTheSame(oldItem: OrderItem, newItem: OrderItem) =
        oldItem.idProduct == newItem.idProduct
}

