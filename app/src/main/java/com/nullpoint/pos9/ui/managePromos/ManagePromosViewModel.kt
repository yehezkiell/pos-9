package com.nullpoint.pos9.ui.managePromos

import androidx.lifecycle.ViewModel
import javax.inject.Inject

@Suppress("EmptyClassBlock")
class ManagePromosViewModel @Inject constructor() : ViewModel()
