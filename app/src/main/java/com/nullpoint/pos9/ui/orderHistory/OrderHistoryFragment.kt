package com.nullpoint.pos9.ui.orderHistory

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.nullpoint.pos9.AppExecutors
import com.nullpoint.pos9.databinding.OrderHistoryFragmentBinding
import com.nullpoint.pos9.di.Injectable
import com.nullpoint.pos9.utils.autoCleared
import javax.inject.Inject

class OrderHistoryFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var viewModel: OrderHistoryViewModel
    var binding by autoCleared<OrderHistoryFragmentBinding>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = OrderHistoryFragmentBinding.inflate(inflater, container, false)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(OrderHistoryViewModel::class.java)

        // TODO: Implement order detail
        val adapter = OrderListAdapter(appExecutors)
        binding.allTransactionRecyclerView.adapter = adapter
        viewModel.orders.observe(this, Observer {
            if (it != null && it.isNotEmpty()) adapter.submitList(it)
        })

        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }
}