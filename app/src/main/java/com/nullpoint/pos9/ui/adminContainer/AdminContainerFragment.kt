package com.nullpoint.pos9.ui.adminContainer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.nullpoint.pos9.R
import com.nullpoint.pos9.di.Injectable
import kotlinx.android.synthetic.main.admin_container_fragment.view.*
import javax.inject.Inject

/**
 *  Top level fragment for admin section. Setups the navigation graph and app bar.
 * */
class AdminContainerFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: AdminContainerViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.admin_container_fragment, container, false)
        val navHost = view.findViewById<View>(R.id.bottom_nav_child_host_fragment)
        val navController = Navigation.findNavController(navHost)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AdminContainerViewModel::class.java)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            view.toolbar_title.text = destination.label
        }
        view.toolbar.setupWithNavController(navController)
        return view
    }
}