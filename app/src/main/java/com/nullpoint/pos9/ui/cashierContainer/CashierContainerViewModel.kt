package com.nullpoint.pos9.ui.cashierContainer

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.nullpoint.pos9.AppExecutors
import com.nullpoint.pos9.repository.OrderRepository
import com.nullpoint.pos9.repository.ProductRepository
import com.nullpoint.pos9.utils.Converter
import com.nullpoint.pos9.vo.OrderItem
import com.nullpoint.pos9.vo.Product
import javax.inject.Inject

class CashierContainerViewModel @Inject constructor(
    productRepository: ProductRepository,
    private val orderRepository: OrderRepository,
    private val appExecutors: AppExecutors
) :
    ViewModel() {
    val products: LiveData<List<Product>> = productRepository.getAllProduct()
    val cart: MutableLiveData<List<OrderItem>> = MutableLiveData<List<OrderItem>>().apply {
        value = emptyList()
    }
    val isCheckoutPossible: LiveData<Boolean> = Transformations.map(cart) {
        cart.value.orEmpty().isNotEmpty()
    }

    val totalPrice: LiveData<String> = Transformations.map(cart) { products ->
        if (products.isNotEmpty()) {
            val totalPrice = products
                .map { it.totalPrice }
                .reduce { acc, i -> acc + i }
            Converter.longToStringCurrency(totalPrice)
        } else {
            Converter.longToStringCurrency(0)
        }
    }

    fun onCheckout(@Suppress("UNUSED_PARAMETER") view: View) {
        appExecutors.diskIO().execute {
            orderRepository.saveOrder(cart)
            appExecutors.mainThread().execute {
                cart.value = emptyList()
            }
        }
    }

    fun addItem(product: Product) {
        val newCart: MutableList<OrderItem> = mutableListOf()
        val itemInCart = cart.value?.find { it.idProduct == product.idProduct }
        itemInCart?.let {
            newCart.add(
                OrderItem(
                    idProduct = product.idProduct,
                    name = product.name,
                    price = product.price,
                    quantity = itemInCart.quantity + 1
                )
            )
            newCart.addAll(cart.value?.filter { it.idProduct != product.idProduct } ?: mutableListOf())
            cart.value = newCart
            return
        }
        newCart.add(OrderItem(idProduct = product.idProduct, name = product.name, price = product.price))
        newCart.addAll(cart.value?.filter { it.idProduct != product.idProduct } ?: mutableListOf())
        cart.value = newCart
    }

    fun updateItem() {
        cart.value = cart.value
    }

    fun removeItem(item: OrderItem) {
        val newCart: MutableList<OrderItem> = mutableListOf()
        newCart.addAll(cart.value?.filter { it.idProduct != item.idProduct } ?: mutableListOf())
        cart.value = newCart
    }
}
