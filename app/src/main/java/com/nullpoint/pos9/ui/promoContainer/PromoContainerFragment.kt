package com.nullpoint.pos9.ui.promoContainer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.nullpoint.pos9.R
import com.nullpoint.pos9.di.Injectable
import javax.inject.Inject

class PromoContainerFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: PromoContainerViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.promo_container_fragment, container, false)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(PromoContainerViewModel::class.java)
        return view
    }
}
