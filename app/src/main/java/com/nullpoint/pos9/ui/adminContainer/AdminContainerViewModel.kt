package com.nullpoint.pos9.ui.adminContainer

import androidx.lifecycle.ViewModel
import javax.inject.Inject

@Suppress("EmptyClassBlock")
class AdminContainerViewModel @Inject constructor() : ViewModel()
