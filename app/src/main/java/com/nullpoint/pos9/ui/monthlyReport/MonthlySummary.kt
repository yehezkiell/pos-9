package com.nullpoint.pos9.ui.monthlyReport

import com.nullpoint.pos9.vo.Order
import org.threeten.bp.YearMonth
import org.threeten.bp.ZoneId

class MonthlySummary(orders: List<Order>, yearMonth: YearMonth?) {
    val yearMonth: YearMonth = yearMonth ?: YearMonth.from(orders.first().createdAt.atZone(ZoneId.systemDefault()))
    val revenue = orders.fold(0L) { acc, order -> acc + order.totalPayAmount }
}
