package com.nullpoint.pos9.ui.newProduct

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.nullpoint.pos9.R
import com.nullpoint.pos9.databinding.NewProductFragmentBinding
import com.nullpoint.pos9.di.Injectable
import com.nullpoint.pos9.utils.EditTextNumberFormatter
import com.nullpoint.pos9.utils.autoCleared
import javax.inject.Inject

class NewProductFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: NewProductViewModel
    var binding by autoCleared<NewProductFragmentBinding>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = NewProductFragmentBinding.inflate(inflater, container, false)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(NewProductViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel
        binding.newPrice.addTextChangedListener(EditTextNumberFormatter(binding.newPrice))
        binding.buttonAddProduct.setOnClickListener {
            val err = viewModel.save()
            if (err.isNullOrEmpty()) {
                findNavController().navigate(R.id.new_product_to_manage_product)
            }
        }
        return binding.root
    }

    override fun onPause() {
        super.onPause()
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
    }
}
