package com.nullpoint.pos9.ui.orderHistory

import androidx.lifecycle.ViewModel
import com.nullpoint.pos9.repository.OrderRepository
import javax.inject.Inject

class OrderHistoryViewModel @Inject constructor(
        orderRepository: OrderRepository
): ViewModel() {
    val orders = orderRepository.getAllOrder()
}