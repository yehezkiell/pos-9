package com.nullpoint.pos9.ui.signIn

import androidx.lifecycle.ViewModel
import javax.inject.Inject

@Suppress("EmptyClassBlock")
class SignInViewModel @Inject constructor() : ViewModel()
