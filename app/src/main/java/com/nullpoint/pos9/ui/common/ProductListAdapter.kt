package com.nullpoint.pos9.ui.common

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.nullpoint.pos9.AppExecutors
import com.nullpoint.pos9.databinding.ProductItemBinding
import com.nullpoint.pos9.vo.Product

/**
 *  Adapter for showing list of products.
 * */
class ProductListAdapter(
    executors: AppExecutors, val callback: ((Product) -> Unit)?
) : DataBoundListAdapter<Product, ProductItemBinding>(
    executors, ProductDiffCallback()
) {
    override fun createBinding(parent: ViewGroup): ProductItemBinding {
        val binding = ProductItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        binding.productContainer.setOnClickListener {
            binding.product?.let { product -> callback?.invoke(product) }
        }
        return binding
    }

    override fun bind(binding: ProductItemBinding, item: Product) {
        binding.product = item
    }
}

private class ProductDiffCallback : DiffUtil.ItemCallback<Product>() {
    override fun areContentsTheSame(old: Product, new: Product) = old == new

    override fun areItemsTheSame(old: Product, new: Product) =
        old.idProduct == new.idProduct
}
