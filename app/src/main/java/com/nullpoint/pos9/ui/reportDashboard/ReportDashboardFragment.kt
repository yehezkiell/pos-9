package com.nullpoint.pos9.ui.reportDashboard

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.nullpoint.pos9.R
import com.nullpoint.pos9.databinding.ReportDashboardFragmentBinding
import com.nullpoint.pos9.di.Injectable
import com.nullpoint.pos9.utils.autoCleared
import org.threeten.bp.Month
import javax.inject.Inject

class ReportDashboardFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ReportDashboardViewModel

    var binding by autoCleared<ReportDashboardFragmentBinding>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = ReportDashboardFragmentBinding.inflate(inflater, container, false)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ReportDashboardViewModel::class.java)
        binding.viewModel = viewModel

        // Update chart when data changed
        viewModel.monthlySummaryChartData.observe(this, Observer {
            binding.monthlyChart.data = it
            binding.monthlyChart.invalidate()
        })

        // setups the chart styling
        binding.monthlyChart.setTouchEnabled(false)
        binding.monthlyChart.legend.isEnabled = false
        binding.monthlyChart.description.isEnabled = false
        binding.monthlyChart.axisRight.isEnabled = false
        binding.monthlyChart.axisLeft.granularity = 1f
        binding.monthlyChart.axisLeft.textColor = Color.WHITE
        binding.monthlyChart.axisLeft.spaceTop = 30f
        binding.monthlyChart.setNoDataTextColor(Color.WHITE)
        binding.monthlyChart.axisLeft.setDrawLabels(false)
        binding.monthlyChart.axisLeft.setDrawAxisLine(false)
        binding.monthlyChart.xAxis.textColor = Color.WHITE
        binding.monthlyChart.xAxis.setDrawGridLines(false)
        binding.monthlyChart.xAxis.setDrawAxisLine(false)
        binding.monthlyChart.xAxis.granularity = 1f
        binding.monthlyChart.xAxis.spaceMin = 0.3f
        binding.monthlyChart.xAxis.spaceMax = 0.3f
        binding.monthlyChart.xAxis.setValueFormatter { value, _ ->
            Month.values()[value.toInt() - 1].toString()
        }

        binding.btnAllTransaction.setOnClickListener(
            Navigation.createNavigateOnClickListener(R.id.report_dashboard_to_order_history)
        )
        binding.monthlyCard.setOnClickListener(
            Navigation.createNavigateOnClickListener(R.id.report_dashboard_to_monthly_report)
        )
        return binding.root
    }
}