package com.nullpoint.pos9.ui.reportDashboard

import android.app.PendingIntent
import android.content.Intent
import android.graphics.Color
import android.view.View
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModel
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.nullpoint.pos9.AppExecutors
import com.nullpoint.pos9.flags.Flags
import com.nullpoint.pos9.repository.OrderRepository
import com.nullpoint.pos9.utils.Converter
import com.nullpoint.pos9.utils.NotificationHelper
import com.opencsv.CSVWriter
import com.shopify.livedataktx.map
import com.shopify.livedataktx.nonNull
import java.io.File
import java.io.FileWriter
import javax.inject.Inject

class ReportDashboardViewModel @Inject constructor(
    private val orderRepo: OrderRepository,
    private val executors: AppExecutors
) : ViewModel() {
    val FILE_NAME = "report.csv"
    private lateinit var helper: NotificationHelper

    // Turns monthlySummary data into
    val monthlySummaryChartData = orderRepo.getMonthlySummary().nonNull().map { summaries ->
        summaries.take(6).map {
            Entry(it.yearMonth.monthValue.toFloat(), it.revenue.toFloat())
        }.run {
            LineDataSet(this, "")
        }.run {
            setCircleColor(Color.WHITE)
            valueTextColor = Color.WHITE
            valueTextSize = 14f
            mode = LineDataSet.Mode.CUBIC_BEZIER
            LineData(this)
        }.apply {
            setValueFormatter { value, _, _, _ ->
                Converter.longToStringCurrency(value.toLong())
            }
        }
    }

    fun exportCsvHandler(view: View) {
        executors.diskIO().execute {

            val ctx = view.context
            helper = NotificationHelper(ctx)
            val mBuilder = helper.getNotificationPrimaryChannel(Flags.NOTIF_MESSAGE_TITLE_EXPORTING, FILE_NAME)
            mBuilder.setProgress(0, 0, true)
            helper.notify(Flags.NOTIF_ID_EXPORT_CSV, mBuilder)

            val orders = orderRepo.getAllOrderSync()

            val file = File(ctx.getExternalFilesDir(null), FILE_NAME)

            val csvWriter = CSVWriter(
                FileWriter(file),
                CSVWriter.DEFAULT_SEPARATOR,
                CSVWriter.NO_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END
            )

            csvWriter.use {
                // set column name and write it to csv
                val header = arrayOf(
                    "idProduct",
                    "idCustomer",
                    "idUser",
                    "totalPayAmount",
                    "tax",
                    "actualPayAmount",
                    "createdAt",
                    "updatedAt",
                    "completed"
                )
                it.writeNext(header)
                for (order in orders) {
                    val data = arrayOf(
                        order.idOrder.toString(), order.idCustomer.toString(), order.idUser.toString(),
                        order.totalPayAmount.toString(), order.tax.toString(), order.actualPayAmount.toString(),
                        order.createdAt.toString(), order.updatedAt.toString(), order.completed.toString()
                    )
                    it.writeNext(data)
                }
            }
            // letting user now their saved file location by notification
            val filePath = file.absolutePath.substring(32)

            val fp = FileProvider.getUriForFile(ctx, ctx.applicationContext.packageName + ".provider", file)
            val intent = Intent(Intent.ACTION_VIEW, fp).apply {
                flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            }

            val pendingIntent: PendingIntent = PendingIntent.getActivity(ctx, 0, intent, 0)
            mBuilder.setProgress(0, 0, false)
                .setContentTitle(Flags.NOTIF_MESSAGE_TITLE_EXPORTED)
                .setContentIntent(pendingIntent)
                .setContentText(filePath)
            helper.notify(Flags.NOTIF_ID_EXPORT_CSV, mBuilder)
        }
    }
}