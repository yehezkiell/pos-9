package com.nullpoint.pos9.ui.manageProducts

import androidx.lifecycle.ViewModel
import com.nullpoint.pos9.repository.ProductRepository
import javax.inject.Inject

class ManageProductsViewModel @Inject constructor(productRepository: ProductRepository) : ViewModel() {
    val products = productRepository.getAllProduct()
}
