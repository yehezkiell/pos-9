package com.nullpoint.pos9.ui.mainContainer

import androidx.lifecycle.ViewModel
import javax.inject.Inject

@Suppress("EmptyClassBlock")
class MainContainerViewModel @Inject constructor() : ViewModel()
