package com.nullpoint.pos9.ui.adminMenu

import androidx.lifecycle.ViewModel
import javax.inject.Inject

@Suppress("EmptyClassBlock")
class AdminMenuViewModel @Inject constructor() : ViewModel()
