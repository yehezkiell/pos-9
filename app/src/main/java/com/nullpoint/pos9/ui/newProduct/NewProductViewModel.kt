package com.nullpoint.pos9.ui.newProduct

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nullpoint.pos9.AppExecutors
import com.nullpoint.pos9.flags.Flags
import com.nullpoint.pos9.repository.ProductRepository
import com.nullpoint.pos9.utils.Converter
import javax.inject.Inject

class NewProductViewModel @Inject constructor(
    private val appExecutors: AppExecutors,
    private val productRepository: ProductRepository
) : ViewModel() {
    var name = ""
    var errorMsg = MutableLiveData<String>()
    var price: MutableLiveData<String> = MutableLiveData()

    fun save(): String? {
        val finalPrice = Converter.commaFormatToString(price.value ?: "0")
        if (name == "" || finalPrice == "") {
            errorMsg.value = Flags.ERROR_MSG_FIELD_NOT_EMPTY
            return errorMsg.value
        } else {
            errorMsg.value = null
        }
        appExecutors.diskIO().execute {
            productRepository.insertToDb(name, finalPrice.toLong(), "")
        }
        return errorMsg.value
    }
}
