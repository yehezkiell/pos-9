package com.nullpoint.pos9.ui.orderHistory

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.nullpoint.pos9.AppExecutors
import com.nullpoint.pos9.databinding.ReportOrderItemBinding
import com.nullpoint.pos9.ui.common.DataBoundListAdapter
import com.nullpoint.pos9.vo.Order

class OrderListAdapter(
    appExecutors: AppExecutors
) : DataBoundListAdapter<Order, ReportOrderItemBinding>(
    appExecutors, OrderDiffUtil()
) {
    override fun createBinding(parent: ViewGroup) = ReportOrderItemBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    )

    override fun bind(binding: ReportOrderItemBinding, item: Order) {
        binding.order = item
    }
}

private class OrderDiffUtil : DiffUtil.ItemCallback<Order>() {
    override fun areItemsTheSame(oldItem: Order, newItem: Order) =
        oldItem.idOrder == newItem.idOrder

    override fun areContentsTheSame(oldItem: Order, newItem: Order) =
        oldItem.updatedAt == newItem.updatedAt
}
