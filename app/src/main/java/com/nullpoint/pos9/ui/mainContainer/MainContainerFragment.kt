package com.nullpoint.pos9.ui.mainContainer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.plusAssign
import androidx.navigation.ui.onNavDestinationSelected
import com.nullpoint.pos9.R
import com.nullpoint.pos9.di.Injectable
import com.nullpoint.pos9.ui.cashierContainer.CashierContainerViewModel
import com.nullpoint.pos9.utils.setSelectedItemId
import kotlinx.android.synthetic.main.main_container_fragment.view.*
import javax.inject.Inject

/**
 *  This is the top level fragment for the app screen, this is where the bottomnav and the navhost that controls which
 *  section (admin, cashier, etc..) that is being shown lives.
 * */
class MainContainerFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: CashierContainerViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.main_container_fragment, container, false)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CashierContainerViewModel::class.java)

        // Get reference to required views
        val navController = Navigation.findNavController(view.findViewById<View>(R.id.home_nav_host_fragment))
        val navHostFragment = childFragmentManager.findFragmentById(R.id.home_nav_host_fragment)

        // setup custom navigator and navController that will prevent child fragment from being destroyed when navigating
        // between sections.
        val navigator = KeepStateNavigator(
            context!!,
            navHostFragment!!.childFragmentManager,
            R.id.home_nav_host_fragment
        )
        navController.navigatorProvider += navigator
        navController.setGraph(R.navigation.home_nav_graph)

        // Bind bottomNav clicks to navigation
        view.bottom_navigation.setOnNavigationItemSelectedListener {
            // Prevent re-navigation to the same fragment when selecting the same item on bottomNav multiple times
            it.itemId != view.bottom_navigation.selectedItemId &&
                it.onNavDestinationSelected(navController) || super.onOptionsItemSelected(it)
        }

        // When navigation navigate from other than bottomNav (eg. back button)
        // Update bottomNav
        navController.addOnDestinationChangedListener { _, destination, _ ->
            view.bottom_navigation.setSelectedItemId(destination.id, false)
        }
        return view
    }
}

