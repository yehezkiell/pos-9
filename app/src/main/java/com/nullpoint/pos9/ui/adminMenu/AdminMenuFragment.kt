package com.nullpoint.pos9.ui.adminMenu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.nullpoint.pos9.R
import com.nullpoint.pos9.di.Injectable
import kotlinx.android.synthetic.main.admin_menu_fragment.view.*
import javax.inject.Inject

/**
 * Top level fragment for admin fragment
 * */
class AdminMenuFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: AdminMenuViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.admin_menu_fragment, container, false)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AdminMenuViewModel::class.java)
        view.edit_product_button.setOnClickListener {
            findNavController().navigate(R.id.admin_menu_to_manage_products)
        }
        return view
    }
}