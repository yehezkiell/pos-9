package com.nullpoint.pos9.ui.cashierContainer

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import android.graphics.drawable.ColorDrawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.nullpoint.pos9.R

/**
 *  Used to detect when an item from recyclerView is swiped away.
 * */
abstract class SwipeDeleteCallback(context: Context) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
    private val deleteIcon = ContextCompat.getDrawable(context, R.drawable.ic_delete_white_24dp)
    private val bgColor = ContextCompat.getColor(context, R.color.darkRed)
    private val intrinsicWidth = deleteIcon!!.intrinsicWidth
    private val intrinsicHeight = deleteIcon!!.intrinsicHeight
    private val background = ColorDrawable()

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        //set icon when item is swiped
        val itemView = viewHolder.itemView
        val itemHeight = itemView.bottom - itemView.top
        val isCanceled = dX == 0f && !isCurrentlyActive

        if (isCanceled) {
            c.drawRect(itemView.right + dX,
                itemView.top.toFloat(), itemView.right.toFloat(), itemView.bottom.toFloat(),
                Paint().apply { xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR) })
            return
        }

        background.color = bgColor
        background.setBounds(itemView.right + dX.toInt(), itemView.top, itemView.right, itemView.bottom)
        background.draw(c)

        //calculate position of delete icon
        val delIconMargin = (itemHeight - intrinsicHeight)
        val delIconTop = itemView.top + (itemHeight - intrinsicHeight) / 2
        val delIconLeft = itemView.right - delIconMargin - intrinsicWidth
        val delIconRight = itemView.right - delIconMargin
        val delIconBottom = delIconTop + intrinsicHeight

        deleteIcon?.setBounds(delIconLeft, delIconTop, delIconRight, delIconBottom)
        deleteIcon?.draw(c)

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }
}