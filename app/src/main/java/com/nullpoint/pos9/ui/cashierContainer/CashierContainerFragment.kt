package com.nullpoint.pos9.ui.cashierContainer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.nullpoint.pos9.AppExecutors
import com.nullpoint.pos9.databinding.CashierContainerFragmentBinding
import com.nullpoint.pos9.di.Injectable
import com.nullpoint.pos9.ui.common.ProductListAdapter
import com.nullpoint.pos9.utils.autoCleared
import kotlinx.android.synthetic.main.cashier_container_fragment.*
import javax.inject.Inject

class CashierContainerFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors
    private lateinit var viewModel: CashierContainerViewModel
    var binding by autoCleared<CashierContainerFragmentBinding>()
    private var allProductListAdapter by autoCleared<ProductListAdapter>()
    private var cartProductsAdapter by autoCleared<CartProductsAdapter>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = CashierContainerFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CashierContainerViewModel::class.java)
        binding.viewmodel = viewModel

        // Setup bottomsheet behaviour (the one that comes up when choosing product)
        val bottomSheetBehavior = BottomSheetBehavior.from(binding.bottomSheet)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        val allProductsAdapter = ProductListAdapter(appExecutors) {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            viewModel.addItem(it)
        }
        this.allProductListAdapter = allProductsAdapter
        binding.allProductRecycler.adapter = allProductsAdapter

        // Setup cart product list
        val cartProductsAdapter = CartProductsAdapter(appExecutors) {
            viewModel.updateItem()
        }
        this.cartProductsAdapter = cartProductsAdapter
        binding.chosenProductRecycler.adapter = cartProductsAdapter

        // Update recyclerview adapters data when data changes
        viewModel.products.observe(this, Observer {
            allProductsAdapter.submitList(it)
        })
        viewModel.cart.observe(this, Observer {
            cartProductsAdapter.submitList(it)
        })

        // Show bottom sheet when "masukan produk" button is clicked
        binding.productInputButton.setOnClickListener {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HALF_EXPANDED
        }

        val swipeHandler = object : SwipeDeleteCallback(context!!) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                //remove product from cart
                viewModel.removeItem(cartProductsAdapter.getItem(viewHolder.adapterPosition))
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(chosen_product_recycler)

        return binding.root
    }
}
