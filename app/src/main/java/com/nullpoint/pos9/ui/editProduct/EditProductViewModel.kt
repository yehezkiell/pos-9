package com.nullpoint.pos9.ui.editProduct

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.nullpoint.pos9.AppExecutors
import com.nullpoint.pos9.flags.Flags
import com.nullpoint.pos9.repository.ProductRepository
import com.nullpoint.pos9.utils.Converter
import com.nullpoint.pos9.vo.Product
import javax.inject.Inject

class EditProductViewModel @Inject constructor(
    private val appExecutors: AppExecutors,
    private val productRepository: ProductRepository
) : ViewModel() {
    val id: MutableLiveData<String> = MutableLiveData()
    val product: LiveData<Product> = Transformations.switchMap(id) {
        productRepository.getProductById(it.toInt())
    }
    val name: LiveData<String> = Transformations.map(product) { it.name }
    val price: LiveData<String> = Transformations.map(product) {
        Converter.stringToCommaFormat(it.price.toString())
    }

    var newName = ""
    var newPrice = MutableLiveData<String>()
    var errorMsg = MutableLiveData<String>()

    init {
        price.observeForever {
            newPrice.value = it
        }
    }

    fun save(): String? {
        val finalPrice = Converter.commaFormatToString(newPrice.value ?: "0")
        if (newName == "" || finalPrice == "") {
            errorMsg.value = Flags.ERROR_MSG_FIELD_NOT_EMPTY
            return errorMsg.value
        } else {
            errorMsg.value = null
        }
        appExecutors.diskIO().execute {
            productRepository.updatePriceById(id.value?.toInt() ?: -1, finalPrice.toInt())
            productRepository.updateNameById(id.value?.toInt() ?: -1, newName)
        }
        return errorMsg.value
    }

    fun setId(id: String) {
        this.id.value = id
    }

    fun onNameChanged(name: CharSequence) {
        newName = name.toString()
    }
}