package com.nullpoint.pos9.ui.monthlyReport

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.nullpoint.pos9.AppExecutors
import com.nullpoint.pos9.databinding.MonthlyReportSummaryItemBinding
import com.nullpoint.pos9.ui.common.DataBoundListAdapter

class MonthlySummaryListAdapter(
    appExecutors: AppExecutors
) : DataBoundListAdapter<MonthlySummary, MonthlyReportSummaryItemBinding>(
    appExecutors, MonthlySummaryDiffUtil()
) {
    override fun createBinding(parent: ViewGroup) = MonthlyReportSummaryItemBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    )

    override fun bind(binding: MonthlyReportSummaryItemBinding, item: MonthlySummary) {
        binding.summary = item
    }
}

class MonthlySummaryDiffUtil : DiffUtil.ItemCallback<MonthlySummary>() {
    override fun areItemsTheSame(oldItem: MonthlySummary, newItem: MonthlySummary) =
        oldItem == newItem

    override fun areContentsTheSame(oldItem: MonthlySummary, newItem: MonthlySummary) =
        oldItem.revenue == newItem.revenue
}