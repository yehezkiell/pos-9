package com.nullpoint.pos9.ui.editProduct

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.nullpoint.pos9.R
import com.nullpoint.pos9.databinding.EditProductFragmentBinding
import com.nullpoint.pos9.di.Injectable
import com.nullpoint.pos9.utils.EditTextNumberFormatter
import com.nullpoint.pos9.utils.autoCleared
import javax.inject.Inject

class EditProductFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: EditProductViewModel
    private val args: EditProductFragmentArgs by navArgs()
    var binding by autoCleared<EditProductFragmentBinding>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = EditProductFragmentBinding.inflate(inflater, container, false)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(EditProductViewModel::class.java)
        viewModel.setId(args.id.toString())
        binding.updatePrice.addTextChangedListener(EditTextNumberFormatter(binding.updatePrice))
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        binding.buttonSaveChanges.setOnClickListener {
            val err = viewModel.save()
            if (err.isNullOrEmpty()) {
                findNavController().navigate(R.id.edit_product_to_manage_products)
            }
        }
        return binding.root
    }

    override fun onPause() {
        super.onPause()
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
    }
}