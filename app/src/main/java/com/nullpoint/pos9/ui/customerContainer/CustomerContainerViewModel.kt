package com.nullpoint.pos9.ui.customerContainer

import androidx.lifecycle.ViewModel
import javax.inject.Inject

@Suppress("EmptyClassBlock")
class CustomerContainerViewModel @Inject constructor() : ViewModel()
