package com.nullpoint.pos9.ui.monthlyReport

import androidx.lifecycle.ViewModel
import com.nullpoint.pos9.repository.OrderRepository
import javax.inject.Inject

class MonthlyReportViewModel @Inject constructor(
    orderRepository: OrderRepository
) : ViewModel() {
    val ordersByMonth = orderRepository.getMonthlySummary()
}
