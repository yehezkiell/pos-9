package com.nullpoint.pos9

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {
    @Inject
    lateinit var dispatchingFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingFragmentInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }

    /**
     * This is sort of a hack, best if we just left all navigation related stuff to navigation component
     *
     * Handles all back button press actions. Due to the complex nav graph shape (3 level deep), back button
     * action is quite complex.
     *
     * NavGraph tree:
     * nav_host_fragment
     * |- home_nav_host
     *      | - bottom_nav_child_host_fragment (3 different one for each app section)
     * |- login_nav_host
     *
     * Expected back button behaviour on home:
     * 1. Navigate up on current chosen section on bottomNav to the bottom of the stack (bottom_nav_child_host_fragment)
     * 2. Navigate up on bottom nav sections (eg. switching from cashier to admin) (home_nav_host)
     * 3. Navigate away from app
     */
    override fun onBackPressed() {
        try {
            // check if on home or login
            val topLevelGraph = findNavController(R.id.nav_host_fragment).currentDestination?.id ?: return

            // if on home, try to navigate up the bottom_nav_child_host_fragment stack
            if (topLevelGraph == R.id.main_container) {
                val successfullyNavigate = findNavController(R.id.bottom_nav_child_host_fragment).navigateUp()
                // if already on the bottom stack, try to navigate up the home_nav_host stack
                if (!successfullyNavigate) {
                    val homeNavController = findNavController(R.id.home_nav_host_fragment)
                    if (homeNavController.currentDestination?.id == R.id.cashier_container) {
                        // if already on cashier, exit activity
                        super.onBackPressed()
                    } else {
                        // if not at cashier, navigate to cashier
                        homeNavController.navigate(R.id.cashier_container)
                    }
                }
            }
        } catch (err: IllegalArgumentException) {
            super.onBackPressed()
        }
    }
}
