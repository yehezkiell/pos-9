package com.nullpoint.pos9.flags

class Flags {
    companion object {
        //database flags
        const val ORDER_STATUS_CREATE = "Create"

        //error message
        const val ERROR_MSG_FIELD_NOT_EMPTY = "Semua field harus diisi"

        //notification message
        const val NOTIF_MESSAGE_TITLE_EXPORTING = "Exporting.."
        const val NOTIF_MESSAGE_TITLE_EXPORTED = "Export completed"

        //notification id
        const val NOTIF_ID_EXPORT_CSV = 1001
    }
}